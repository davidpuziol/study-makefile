# 5 - Funções

Funções podems ser algo que já existe ou personalizadas.

Para declarar uma função personalizada vema o exemplo abaixo.

```makefile
define FUNCAO
	@echo "Olá $(1). Bem vindo ao $(2)"
endef

all:
# call + FUNCAO,param1,param2,param3,etc
	$(call FUNCAO,"Fulano","study-makefile!")
```

Oa parâmetros são acessados por `$(1) $(2) $(3) $(4) ... ` de acordo com a ordem de entrada.

As funções personalizadas devem ser definidas antes da sua chamada. Para chamar uma função use o comando call + nome da funcao,param1,param2,param3.

> Uma função pode receber quantos paramtros quiser mesmo se não utilizá-los. Um bom uso de função é para executar algum tipo de teste caso o código tenha.

Para referenciar uma função nativa, utilizar a chamada da função e os parâmetros denttro de `$()`.

Função de `addprefix`, adiciona algo antes do nome
Função de `addsufix`, adidciona algo após o nome
Função `wildcard`, seleciona varios nome podendo usar o coringa *.
Função `shell`, executa um comando do shell pega o output de saida. Essa é uma função muito útil para executar praticamente todos os comandos do linux

Prefixo e sufixo são muito interessante para criar versões. Por exemplo se adicionarmos um prefixo sempre antes do nome com o valor da hora. Ou na saída do programa colocasrmos um .exe.

Analise o makefile e veja funções funcionando

```makefile
#Para escrever comentários ##
############################# Makefile ##########################
#Definimos a variável

# Utilizado a função shell para executar comandos do shell, nesse caso pegamos a data
DATE = $(shell date --iso=ns)

CC = gcc
CFLAGS = -W -Wall -ansi -pedantic
EXEC = programa
EXT = .exe

SRCDIR = ./src/
LIBDIR = ./lib/
BUILDDIR = ./build/
OBJDIR = ./obj/

# Aqui utilizado a função wildcard para poder pegar todos os arquivos nas variaveis
SRCS = $(wildcard $(SRCDIR)*.c)

OBJ = $(SRCS:$(SRCDIR)%.c=$(OBJDIR)%.o)

all:	$(EXEC)
	@echo "Fim da compilação as $(shell date --iso=ns)"

$(EXEC):	$(OBJ)

# Utilizado a função suffix para adicionar .exe ao fim do arquivo para saber que é um executavel
	$(CC) -o $(BUILDDIR)$(addsuffix $(EXT), $@) $^ 

$(OBJDIR)%.o: 	$(SRCDIR)%.c
	$(CC) -o $@ -c $< $(CFLAGS)

$(OBJDIR)main.o: 	$(SRCDIR)main.c $(LIBDIR)teste.h
	$(CC) -o $@ -c $< $(CFLAGS)

.PHONY: clean mrproper

clean: 
	@rm -rf $(OBJDIR)*.o
	@echo "Objetos Deletados" 
mrproper:	clean
	@rm -rf $(BUILDDIR)*$(EXT)
	@echo "Executaveis Deletados" 
```

Execute agora os comandos e veja como fica as saídas nas pastas corretas e observe o programa.exe na pasta build.

```bash
❯ make 
gcc -o obj/main.o -c src/main.c -W -Wall -ansi -pedantic
gcc -o obj/teste.o -c src/teste.c -W -Wall -ansi -pedantic
gcc -o ./build/programa.exe obj/main.o obj/teste.o 
Fim da compilação as 2022-11-21T13:38:51,593632200-03:00

❯ ls -lha build 
total 28K
drwxrwxr-x 2 dprata dprata 4,0K nov 21 13:38 .
drwxrwxr-x 6 dprata dprata 4,0K nov 21 13:36 ..
-rwxrwxr-x 1 dprata dprata  17K nov 21 13:38 programa.exe

❯ ls -lha obj 
total 16K
drwxrwxr-x 2 dprata dprata 4,0K nov 21 13:38 .
drwxrwxr-x 6 dprata dprata 4,0K nov 21 13:36 ..
-rw-rw-r-- 1 dprata dprata 1,6K nov 21 13:38 main.o
-rw-rw-r-- 1 dprata dprata 1,7K nov 21 13:38 teste.o

❯ make mrproper 
Objetos Deletados
Executaveis Deletados
```

