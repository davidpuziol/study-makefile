# 2 - Dependências

Nesse projeto vamos aprender como utilizar dependencias no Makefile.

Quando se tem uma dependência, se ele não encontrar o arquivo com o nome da dependência, ele precisa saber como ele irá reproduzir este alvo. Logo no nosso exemplo ele executará antes o alvo mensagem1.

Se um alvo executir, mas não for uma dependência como mensagem2, não será executado.

````bash
# Vamos ver o que ele faria.
❯ make -n
# vai executar a dependência, pois não encontrou o alvo
echo "Olá sou a mensagem 1"
# agora o comando final
echo "Olá mundo, vamos aprender Makefile!"
````

Agora vamos direto no alvo que queremos.

````make
❯ make mensagem2
Olá sou a mensagem 2

❯ make mensagem1  
Olá sou a mensagem 1


````

e a saída será

````bash
❯ ls
total 16K
drwxr-xr-x 2 david david 4.0K Nov 20 22:53 .
drwxr-xr-x 8 david david 4.0K Nov 20 22:53 ..
-rw-r--r-- 1 david david  305 Nov 20 23:05 Makefile
-rw-r--r-- 1 david david  791 Nov 20 23:04 README.md

❯ make criadir
mkdir diretorio

❯ ls
total 20K
drwxr-xr-x 3 david david 4.0K Nov 20 23:05 .
drwxr-xr-x 8 david david 4.0K Nov 20 22:53 ..
-rw-r--r-- 1 david david  305 Nov 20 23:05 Makefile
-rw-r--r-- 1 david david  791 Nov 20 23:04 README.md
drwxr-xr-x 2 david david 4.0K Nov 20 23:05 diretorio
````

O que aconteceria ser tivesse o arquivo mensagem1

````bash
❯ touch mensagem1
❯ make
Olá mundo, vamos aprender Makefile!
````

Esse arquivo não tem nada, por isso não aconteceu nada, mas não executou o alvo mensagem1.

# Dependencia Avançado

para entender um pouco melhor vamos mais a fundo.

Como falado anteriormente precisamos disso:

````make
Alvo:   Dependência
    Comando
````

- `Alvo`: O ques será produzido?
- `Dependência`: Do que eu preciso para produzir o Alvo?
- `Comando`: Como a partir da dependência, vou produzir o Alvo?

Vamos compilar um programa com dependências:

>A ordem das dependências faz diferença em alguns casos.

````make
# Para criar o programa precisamos de teste.o e main.o
# A ordem faz diferença em alguns casos, logo, confira bem isso
programa:   teste.o main.o 
	gcc -o programa teste.o main.o
# Para criar o teste.o precisamos de teste.c
teste.o:	teste.c
	gcc -o teste.o -c teste.c -W -Wall -ansi -pedantic
# Para criar o main.o precisamos de main.c e teste.h
main.o:		main.c teste.h
	gcc -o main.o -c main.c -W -Wall -ansi -pedantic
````

Vamos criar um arquivo chamado Makefile2 e vamos fazer um make em cima dele.

> Para executar um make em cima de um arquivo que não chama-se Makefile precisamos passar o nome o path para o file com o -f

````bash
❯ make -f Makefile2 -n
gcc -o teste.o -c teste.c -W -Wall -ansi -pedantic
gcc -o main.o -c main.c -W -Wall -ansi -pedantic
gcc -o programa teste.o main.o
````

Olha o que vai acontecer, ele primeiro vai executar as duas dependências para depois executar o programa. Ele vai empilhando as dependências.

````bash
# Se não for passado o alvo, ele irá executar todos, nesse caso eu passei o específico, mas nesse caso daria no mesmo se não passasse programa.
❯ make -f Makefile2 programa 
gcc -o teste.o -c teste.c -W -Wall -ansi -pedantic
gcc -o main.o -c main.c -W -Wall -ansi -pedantic
gcc -o programa teste.o main.o

❯ ls -lha
total 64K
drwxr-xr-x 3 david david 4.0K Nov 21 01:00 .
drwxr-xr-x 8 david david 4.0K Nov 20 22:53 ..
-rw-r--r-- 1 david david  305 Nov 20 23:05 Makefile
-rw-r--r-- 1 david david  452 Nov 21 00:56 Makefile2
-rw-r--r-- 1 david david 3.1K Nov 21 01:01 README.md
drwxr-xr-x 2 david david 4.0K Nov 20 23:05 diretorio
-rw-r--r-- 1 david david  171 Nov 21 00:46 main.c
-rw-r--r-- 1 david david 1.6K Nov 21 01:00 main.o # gerado
-rwxr-xr-x 1 david david  17K Nov 21 01:00 programa # gerado
-rw-r--r-- 1 david david  184 Nov 21 00:46 teste.c
-rw-r--r-- 1 david david  248 Nov 21 00:46 teste.h
-rw-r--r-- 1 david david 1.7K Nov 21 01:00 teste.o # gerado
````

