# 1 - Olá mundo
Nesse pequeno projeto inicial vamos aprender a criar um olá mundo simples`

Execute o comando make na pasta desse projeto no terminal.

````bash
❯ make
# Primeiro ele escreveu o comando que ele ia executar depois executou o comando
echo "Olá mundo, vamos aprender Makefile!"
Olá mundo, vamos aprender Makefile!
````

Se  quiser executar um comando sem que ele antes mostre o que será executado coloque `@` na frente do comando como mostra abaixo para uma execução silenciosa. Altere o arquivo desta maneira e faça o teste novamente.

````make
all:
    @echo "Olá mundo, vamos aprender Makefile!"
````

e a saída será

````bash
❯ make
Olá mundo, vamos aprender Makefile!
````

Para nível de depuração do Makefile, podemos usar a flag -n para ele avisar qual o comando irá executar, mas sem executar.

````bash
❯ make -n 
echo "Olá mundo, vamos aprender Makefile!"
````