# 6 - Condicionais

As condicionais no make são simples e capadas. Para isso é necessário usar if dentro de if para gerar uma condicional `AND` por exemplo

Existem sim condicionais while, for por exemplo dentro do makefile, mas tornaria uma coisa que deveria ser simples complexa. Não adianta fazer um conteúdo inteiro sobre isso, por que todos nós esquecemos, mas o if acredito que é bom saber como funciona, pois é sempre o mais usado.

Foi feito uma exemplo pensando que poderia existir diferentes arquiteturas na máquina que está rodando o make ou caso fosse necessário escolher uma arquitetura.
Não foi instalado de fato um gcc para cada uma das arquiteturas, logo foi chamado o mesmo, mas poderia ser definido variasveis CCLINUX CCWINDOWS CCMAC.

A proposta é mostrar o uso de ifs

```makefile
DATE = $(shell date --iso=ns)

# Buscando a arquitetura do sistema
MACHINE = $(shell uname -m)

CC= gcc
CFLAGS = -W -Wall -ansi -pedantic
EXEC = programa
EXT = .exe

SRCDIR = ./src/
LIBDIR = ./lib/
BUILDDIR = ./build/
OBJDIR = ./obj/

# Aqui utilizado a função wildcard para poder pegar todos os arquivos nas variaveis
SRCS = $(wildcard $(SRCDIR)*.c)

OBJ = $(SRCS:$(SRCDIR)%.c=$(OBJDIR)%.o)

all:	$(EXEC)
	@echo "Fim da compilação as $(shell date --iso=ns)"

$(EXEC):	$(OBJ)
ifeq ($(MACHINE), i386)
		@echo "Compilando para i386"
		# Aqui poderia ter sido colocado processo diferente
		$(CC) -o $(BUILDDIR)$(addsuffix $(EXT), $@) $^
else ifeq ($(MACHINE), x86_64)
		@echo "Compilando para x86_64"
		$(CC) -o $(BUILDDIR)$(addsuffix $(EXT), $@) $^
else ifeq ($(MACHINE), arm)
		# Aqui poderia ter sido colocado processo diferente
		@echo "Compilando para arm"
		$(CC) -o $(BUILDDIR)$(addsuffix $(EXT), $@) $^
endif

# Da mesma forma, poderia ter sido colocado ifs aqui dentro para avaliar a arquitetura e buildar de acordo com a definida em MACHINE com os parametros corretos.

$(OBJDIR)%.o: 	$(SRCDIR)%.c
	$(CC) -o $@ -c $< $(CFLAGS)

$(OBJDIR)main.o: 	$(SRCDIR)main.c $(LIBDIR)teste.h
	$(CC) -o $@ -c $< $(CFLAGS)

.PHONY: clean mrproper

clean: 
	@rm -rf $(OBJDIR)*.o
	@echo "Objetos Deletados" 
mrproper:	clean
	@rm -rf $(BUILDDIR)*$(EXT)
	@echo "Executaveis Deletados" 
```

Vamos rodar o codigo

```bash
# Nesse caso a váriavel MACHINE será definido localmente dentro do sistem
❯ make 
Compilando para x86_64
gcc -o ./build/programa.exe obj/main.o obj/teste.o
Fim da compilação as 2022-11-21T15:17:03,258607832-03:00

# Nesse caso é passado a variavel MACHINE com o seu valor
❯ make MACHINE=i386
Compilando para i386
gcc -o ./build/programa.exe obj/main.o obj/teste.o
Fim da compilação as 2022-11-21T15:17:05,557738755-03:00

❯ make MACHINE=arm 
Compilando para arm
gcc -o ./build/programa.exe obj/main.o obj/teste.o
Fim da compilação as 2022-11-21T15:17:07,786289821-03:00
```
