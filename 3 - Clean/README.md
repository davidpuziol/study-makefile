# 3 - Clean

A única parte do projeto que vai ser interessante é incluir o clean no [projeto de dependências](../2%20-%20Dependencias/)

Incluir esse trecho

````make
# Esse alvo não terá dependências, somente executará quando chamado
clean:
# Comando que remove todo os aquivos .o
	rm -rf *.o
	
# Nesse caso ele chama o clean para remover todos os .o e remove também o executavel programa
mrproper: clean
	rm -rf programa
````

````bash
# Vamos ver o que ele faria.
❯ make -n
# vai executar a dependência, pois não encontrou o alvo
echo "Olá sou a mensagem 1"
# agora o comando final
echo "Olá mundo, vamos aprender Makefile!"
````

````bash
❯ make programa
gcc -o teste.o -c teste.c -W -Wall -ansi -pedantic
gcc -o main.o -c main.c -W -Wall -ansi -pedantic
gcc -o programa teste.o main.o

# Arquivos gerados
❯ ls -lha          
total 56K
drwxr-xr-x 2 david david 4.0K Nov 21 01:11 .
drwxr-xr-x 9 david david 4.0K Nov 21 01:04 ..
-rw-r--r-- 1 david david  506 Nov 21 01:10 Makefile
-rw-r--r-- 1 david david 3.7K Nov 21 01:09 README.md
-rw-r--r-- 1 david david  171 Nov 21 01:04 main.c
-rw-r--r-- 1 david david 1.6K Nov 21 01:11 main.o #esse
-rwxr-xr-x 1 david david  17K Nov 21 01:11 programa #esse
-rw-r--r-- 1 david david  184 Nov 21 01:04 teste.c
-rw-r--r-- 1 david david  248 Nov 21 01:04 teste.h
-rw-r--r-- 1 david david 1.7K Nov 21 01:11 teste.o #esse

❯ make clean 
rm -rf *.o

❯ ls -lha
total 48K
drwxr-xr-x 2 david david 4.0K Nov 21 01:11 .
drwxr-xr-x 9 david david 4.0K Nov 21 01:04 ..
-rw-r--r-- 1 david david  506 Nov 21 01:10 Makefile
-rw-r--r-- 1 david david 3.7K Nov 21 01:09 README.md
-rw-r--r-- 1 david david  171 Nov 21 01:04 main.c
#Observe que não removeu o programa
-rwxr-xr-x 1 david david  17K Nov 21 01:11 programa
-rw-r--r-- 1 david david  184 Nov 21 01:04 teste.c
-rw-r--r-- 1 david david  248 Nov 21 01:04 teste.h

❯ make programa 
gcc -o teste.o -c teste.c -W -Wall -ansi -pedantic
gcc -o main.o -c main.c -W -Wall -ansi -pedantic
gcc -o programa teste.o main.o

# Removeu além dos .o o executavel program
❯ make mrproper 
rm -rf *.o
rm -rf programa

❯ ls -lha
total 28K
drwxr-xr-x 2 david david 4.0K Nov 21 01:12 .
drwxr-xr-x 9 david david 4.0K Nov 21 01:04 ..
-rw-r--r-- 1 david david  506 Nov 21 01:10 Makefile
-rw-r--r-- 1 david david 3.7K Nov 21 01:09 README.md
-rw-r--r-- 1 david david  171 Nov 21 01:04 main.c
-rw-r--r-- 1 david david  184 Nov 21 01:04 teste.c
-rw-r--r-- 1 david david  248 Nov 21 01:04 teste.h
````

Geralmente quando precisa gerar somente o executável, vc pode remover todos os .o, logo poderia depois do que o executável acontecesse. como faze-lo?

depois no program poderia ser adicionado o make clean no final

````make
programa:   teste.o main.o
	gcc -o programa teste.o main.o
	make clean
# Para criar o teste.o precisamos de teste.c
teste.o:	teste.c
	gcc -o teste.o -c teste.c -W -Wall -ansi -pedantic
# Para criar o main.o precisamos de main.c e teste.h
main.o:		main.c teste.h
	gcc -o main.o -c main.c -W -Wall -ansi -pedantic

clean:
	rm -rf *.o

mrproper:	clean
	rm -rf programa
````
