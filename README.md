# Study Makefile

[Documentação oficial](https://www.gnu.org/software/make/manual/make.html) deve ser a fonte de estudo principal em caso de dúvidas.

O objetivo de Makefile é definir regras de compilação para projetos de software. Tais regras são definidas em arquivo chamado Makefile. O programa make interpreta o conteúdo do Makefile e executa as regras lá definidas.. O programa `make`  pode variar de um sistema sistema operacional outro (tais como gmake, nmake, tmake) pois não faz parte de nenhuma normalização .## Conceitos iniciais

O texto contido em um Makefile é usado para a compilação, ligação(linking), montagem de arquivos de projeto entre outras tarefas como limpeza de arquivos temporários, execução de comandos, etc.

## Vantagens do uso do Makefile

- Evita a compilação de arquivos desnecessários. Por exemplo, se seu programa utiliza 120 bibliotecas e você altera apenas uma, o make descobre (comparando as datas de alteração dos arquivos fontes com as dos arquivos anteriormente compilados) qual arquivo foi alterado e compila apenas a biblioteca necessária. 
- Automatiza tarefas rotineiras como limpeza de vários arquivos criados temporariamente na compilação.
- Pode ser usado como linguagem geral de script embora seja mais usado para compilação.

## Resumo Geral sobre Makefile

Veja o exemplo um básico sobre makefiles em [Resumo Makefile](./Resumo%20Makefile/)

## Compilação

O processador só entende binário 0 e 1 e os seres humanos tem dificuldade de programar diretamente assim.

Para isso são definidas linguagens de alto nível que criam um algoritmo (fluxo lógico) que consegue ser lido facilmente por seres humanos.

O compilador (outros software) le esta linguagem de alto nível e transcreve para binário.

As linguagens pode ser

- Compiladas (C, C++, Delphi, Python, Rust, etc)
  - Pega o código fonte de alto nível e gera o executável que já está em linguagem de máquina (0 e 1)
  - É necessário recompilar em cada mudança do código fonte
  - É necessário compilar para a arquitetura correta do processador (Compilação cruzada (cross-compiling). Quando compilada para o uso na mesma máquina ou arquitetura da máquina de desenvolvimento chama-se compilação nativa.
  - Costuma ser linguagens mais rápidas por já estar tudo pronto.
  
- Código Intermediário (Java, C#)
  - Gera um código intermediário para rodar sobre uma máquina virtual que é preparado para ler esse pré compilado e compilar corretamente para a arquitetura do processador que a máquina virtual está rodando, fazendo os ajustes necessários e até melhorias.
  - Costuma otimizar o código para a compilação final de acordo com o processador sendo uma vantagem sobre o código interpretado.
  - Linguagem se preocupa em gerar máquinas virtuais para cada arquitetura de processador.
  - Maior flexibilidade
  - É mais fácil de fazer a engenharia reversa com o código pré compilado gerando um problema de segurança.
  
- Interpretada (PHP, Java Script, Beanshell, etc)
  - Le o código fonte em tempo de execução e gera o código de máquina em tempo real.
  - Não necessita gerar executáveis preparados para cada arquitetura
  - Menos performático

  > O compilador verifica erro de sintaxer, mas não erro de lógica.

  ![compitlação](./pics/compile.png)

## Como funciona um Makefile?

![makefile](./pics/makefile.png)

Um makefile é um arquivo (por padrão chamado de “Makefile”) contendo um conjunto de diretivas usadas pela ferramenta de automação de compilação `make` para gerar um alvo/meta(instalar, desinstalar, remover alguns arquivos e outros).

Um makefile contém essencialmente atribuições de variáveis, comentários e regras (“targets”). Comentários são iniciados com o carácter “#”.

Você pode usar um editor de texto para escrever seu Makefile, no entanto, é necessário ficar atento aos detalhes, pois o Makefile gera erro até com espaços onde deveria ser TAB. Vejamos algumas linhas:

Seguir os projetos abaixo facilita no aprendizado

[1 - Olá Mundo](./1%20-%20Ola%20mundo/)\
[2 - Dependencias](./2%20-%20Dependencias/)\
[3 - Clean](./3%20-%20Clean/)\
[4 - Variaveis e Mascaras](./4%20-%20Variaveis%20e%20Mascaras/)\
[5 - Funções](./5%20-%20Funcoes/)\
[6 - Build Condicional](./6%20-%20Build%20Condicional/)