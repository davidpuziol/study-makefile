# 4 - Variáveis

Imagine ter que modificar uma linha e ter que ir em todos os pontos do makefile modificando aquela mesma linha? Muito trabalho.

As variáveis servem para facilitar o trabalho. Em vez de mudar varias linhas mudamos só o conteúdo da variável.

Como usar:

````make
 NOME=CONTEÚDO
# E para utilizar esta variável colocamos entre $() .
# Então ela vai ficar assim $(NOME)
 ````

 >Variráveis são usadas em MAIUSCULO

 Uma coisa importqante é que temos duas variávies específicas chamadas de máscaras.

- `*.o` = Qualquer .o no comando.
  - Pode ser trocado por outra extensão ex: \*.java, \*.cpp, etc
  - Só pode ser utilizado nos comandos, nunca nos alvos e dependências
- `%.o` = Qualquer .o no alvo ou dependência
  - Não pode ser usado nos comandos
- `$@` = Rerefencia o nome do alvo:
- `$^` = Referência a todas as dependências do alvo sem duplicação separado por espaços
  - `$+`= Mesma coisa porém com duplicação
- `$<` = Referência a primeira dependência do alvo
- `$%` = O elemento de nome de arquivo de uma especificação de membro de arquivo.
- `$?`= os nomes de todas as dependências que são mais recentes que o alvo, separados por espaços.

- `.PHONY` Preste bem atenção. Esta regra permite de evitar conflitos.
Por exemplo "clean:" e um alvo sem nenhuma dependência não temos nada na pasta que se chame clean. Se colocar na pasta um arquivo chamado clean tentar apagar os "arquivos.o" escrevendo "make clean" não vai acontecer nada porque make diz que clean não foi modificado.
  - Para evitar esse problema usamos a regra .PHONY
  - .PHONY: diz que clean e mrproper devem ser executados mesmo se arquivos com esses nomes existem.

Existem outras variáveis especiais, porém as 3 primeiras são as mais usadas. Consulte <https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html#Automatic-Variables>

all: library.cpp main.cpp

Vamos aprimorar agora o nosso Makefile

````make
#Para escrever comentários ##
############################# Makefile ##########################
#Definimos a variável
CC=gcc
CFLAGS=-W -Wall -ansi -pedantic
EXEC=programa

# 1 opção manual
#OBJ=teste.o main.o

#2 opção manual usado para poucos arquivos
# Os objetos seriam o fonte, porém aou invés de .c é .o
#SRC = main.c teste.c 
#OBJ= $(SRC:.c=.o) 

# 3 Opção usado para muitos arquivos. 
#Utilizar o *c não funcionaria pois não podemos usar o caractere joker na definição de uma variável por isso vamos usar o comando wildcard
SRC= $(wildcard *.c)
OBJ= $(SRC:.c=.o)

all: $(EXEC)
	@echo "Vou começar a compilação"

programa:	$(OBJ)
# $@ = programa:
# $^ = teste.o main.o
	$(CC) -o $@ $^ 
## Seria a mesma coisa que digitar:
# gcc -o programa main.o teste.o

# teste.o:teste.c

%.o: 	%.c
	$(CC) -o $@ -c $< $(CFLAGS)
# Seria a mesma coisa que digitar
# gcc -o main.o -c main.c  -W -Wall -ansi -pedantic
# gcc -o teste.o -c teste.c  -W -Wall -ansi -pedantic
# Nesse caso funcionaria como um loop sendo que os % pegam os mesmo nomes

# Só para pagarantir que o arquivo teste.h também esteja na pasta poderia ser adicionado a llinha abaixo como uma recompilação garantindo o teste.h, mas nao seria necessário
main.o: 	main.c teste.h
	$(CC) -o $@ -c $< $(CFLAGS)

# .Aqui declaramos que não precisa de arquivos ou dependências para clean e mrproper
.PHONY: clean mrproper

clean: 
	@rm -rf *.o
	@echo "Objetos Deletados" 
mrproper:	clean
	@rm -rf $(EXEC)
	@echo "Executado Deletado" 
````

> Quando reclaramos o `programa:	$(OBJ)` e dentro de OBJ colocamos todos os arquivos, `OBJ= $(SRC:.c=.o)` em alguns casos a ordem importa, logo é necessário fazer manualmente. Não é o caso aqui, mas é possível que aconteça.

O uso do wildcard somente faz com que seja possível utilizar * dentro das variáveis.

````bash
❯ make         
gcc -o main.o -c main.c -W -Wall -ansi -pedantic
gcc -o teste.o -c teste.c -W -Wall -ansi -pedantic
gcc -o programa main.o teste.o 
Vou começar a compilação

❯ ./programa   
O Makefile é super Legal
````

> A minha dica é ter cautela no uso de máscaras para que todo mundo entenda. Às vezes é melhor usar somente alguns recursos mais utilizados para que toda equipe conheça e possa gerenciar o Makefile.
